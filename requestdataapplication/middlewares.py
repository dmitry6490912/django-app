from collections import defaultdict

from django.http import HttpRequest, HttpResponseForbidden
from django.utils import timezone

from module_13 import settings


def setup_useragent_on_request_middleware(get_response):
    print("Initial call")

    def middleware(request: HttpRequest):
        print("before get response")
        user_agent = request.META.get("HTTP_USER_AGENT")
        if user_agent:
            request.user_agent = user_agent
        else:
            request.user_agent = "Unknown"
        response = get_response(request)
        print("after get response")
        return response

    return middleware


class CountRequestsMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.requests_count = 0
        self.responses_count = 0
        self.exceptions_count = 0

    def __call__(self, request: HttpRequest):
        self.requests_count += 1
        print("requests count", self.requests_count)
        response = self.get_response(request)
        self.responses_count += 1
        print("responses count", self.responses_count)
        return response

    def process_exception(self, request: HttpRequest, exception: Exception):
        self.exceptions_count += 1
        print("got", self.exceptions_count, "exceptions so far")


class ThrottlingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.request_log = defaultdict(list)

    def __call__(self, request: HttpRequest):
        ip_address = request.META.get("REMOTE_ADDR")

        current_time = timezone.now()

        if ip_address in self.request_log:
            last_request_time = self.request_log[ip_address][-1]
            time_since_last_request = current_time - last_request_time
            if time_since_last_request.total_seconds() < settings.THROTTLING_INTERVAL:
                return HttpResponseForbidden("So much requests!", status=403)

            self.request_log[ip_address].append(current_time)
        else:
            self.request_log[ip_address] = [current_time]

        response = self.get_response(request)
        return response


