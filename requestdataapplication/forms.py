from django import forms
from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import InMemoryUploadedFile


class UserBioForm(forms.Form):
    first_name = forms.CharField(label="Your name", max_length=15)
    last_name = forms.CharField(label="Your surname", max_length=30)
    age = forms.IntegerField(label="Your age", min_value=1, max_value=120)
    bio = forms.CharField(label="Biography", widget=forms.Textarea, max_length=2000)


def validate_file_name(file: InMemoryUploadedFile) -> None:
    if file.name and "virus" in file.name:
        raise ValidationError("file name would not contained word 'virus'")


class UploadFileForm(forms.Form):
    myfile = forms.FileField(validators=[validate_file_name])
