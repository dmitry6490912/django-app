from module_20 import settings
from django.core.files.storage import FileSystemStorage
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import render
from .forms import UserBioForm, UploadFileForm


def process_get_view(request: HttpRequest) -> HttpResponse:
    a = request.GET.get("a", "")
    b = request.GET.get("b", "")
    result = a + b
    context = {
        "a": a,
        "b": b,
        "result": result,
    }
    return render(request, "requestdataapplication/request-query-params.html", context=context)


def user_form(request: HttpRequest) -> HttpResponse:
    context = {
        "form": UserBioForm,
    }
    return render(request, "requestdataapplication/user-bio-form.html", context=context)


def handle_file_upload(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            myfile = request.FILES["myfile"]
            if myfile.size <= settings.MAX_FILE_SIZE:
                fs = FileSystemStorage()
                filename = fs.save(myfile.name, myfile)
                print("Saved file:", filename, "uploaded successfully")
                return JsonResponse({"Message": f"File {myfile.name} uploaded successfully"}, status=200)
            else:
                return JsonResponse({"Message": f"Upload file more than {settings.MAX_FILE_SIZE} MB"})
        else:
            return JsonResponse({"Message": "The form was not validated"}, status=400)
    elif request.method == "GET":
        form = UploadFileForm()
        return render(request, "requestdataapplication/file-upload.html", {"form": form})
    else:
        return JsonResponse({"Message": "Invalid request"}, status=400)