from blogapp.sitemap import BlogSiteMap
from shopapplication.sitemap import ShopSitemap

sitemaps = {
    "blog": BlogSiteMap,
    "shop": ShopSitemap,
}

