��    &      L  5   |      P  �   Q  �   �  �   ]     �     �     �                3  
   H     S     `     q     }     �     �     �     �     �     �     �     �     �     �     �     �        	             !     .     =  -   A     o     {     �     �  �  �    L  �   Q	  �   >
  #   �
     �
  +     +   ?  $   k  $   �     �     �     �     �          #     :  4   A  #   v  
   �     �     �     �     �  
   �     �          $     @     Q     `     |     �  <   �     �  >   �     -     >         	              "                                                  $   %                                    #                      
          &               !                   
                    There is only one image.
                     
                    There are %(image_count)s images.
                 
                There is only one product.
                 
                There are %(product_count)s products.
             
        <h3>No products yet</h3>
            <a href="%(create_product_url)s"
                >Create a new one</a>
             Archive product Archived Back to order list Back to product list Create a new order Create a new product Created by Delete order Delivery address Description Discount Images No No images uploaded yet No orders yet Order Order by Orders Orders list Price Product Products Products in order Products list Promocode Today is Update order Update product Yes You don't have permission to create products. no discount one product {count} products product nameName welcome hello world! Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 
                Доступно %(image_count)s изображение.
             
                Доступно %(image_count)s изображения.
             
                Доступно %(image_count)s изображений.
             
                Доступен %(product_count)s товар.
             
                Доступно %(product_count)s товара.
             
                Доступно %(product_count)s товаров.
             
        <h3>Пока нет товаров</h3>
            <a href="%(create_product_url)s"
                >Создать новый</a>
             Архивировать товар Архивирован Назад к списоку заказов Назад к списоку товаров Создать новый заказ Создать новый товар Создан Удалить заказ Адрес доставки Описание товара Скидка Изображения Нет Нет загруженных изображений Заказы отсутствуют Заказ Заказ создан Заказы Список заказов Цена Товар Товары Товары в заказе Список товаров Промокод Сегодня Обновить заказ Обновить товар Да У вас нет прав на создание товара нет скидки {count} товар {count} товара {count} товаров Название Привет, Мир! 