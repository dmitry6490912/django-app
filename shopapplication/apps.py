from django.apps import AppConfig


class ShopapplicationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shopapplication'
