from typing import Sequence

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from shopapplication.models import Product


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Start demo bulk actions")
        result = Product.objects.filter(
            name__contains="Smartphone",
        ).update(discount=10)
        print(result)
        # info = [
        #     ("Smartphone Xiaomi 12 Pro", 220),
        #     ("Smartphone Xiaomi 13 T", 289),
        #     ("Smartphone Apple 15", 1100),
        # ]
        # products = [
        #     Product(name=name, price=price)
        #     for name, price in info
        # ]
        # result = Product.objects.bulk_create(products)
        # for objct in result:
        #     print(objct)
        self.stdout.write("Done")
