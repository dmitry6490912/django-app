from django.core.management import BaseCommand
from shopapplication.models import Product


class Command(BaseCommand):
    """
    Creates products
    """

    def handle(self, *args, **options):
        self.stdout.write("Create products")
        products_names = [
            "Laptop",
            "Desktop",
            "Smartphone",
            "Mouse",
            "Camera 'Tapo C310'",
            "USB Cable",
        ]
        for products_name in products_names:
            product, created = Product.objects.get_or_create(name=products_name)
            self.stdout.write(f"Created product {product.name}")

        self.stdout.write(self.style.SUCCESS("Products created"))
