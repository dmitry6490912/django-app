"""
В этом модуле лежат различные наборы представлений.

Разные view для интернет-магазина: по товарам, заказам и т.д.
"""
import logging
from timeit import default_timer
from csv import DictWriter

from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        PermissionRequiredMixin,
                                        UserPassesTestMixin)
from django.contrib.auth.models import Group, User
from django.contrib.syndication.views import Feed
from django.core.cache import cache
from django.db.models import Sum, F, ExpressionWrapper, DecimalField
from django.db.models.functions import Coalesce
from django.http import (HttpResponse, HttpRequest,
                         HttpResponseRedirect, JsonResponse)
from django.shortcuts import (render, redirect, reverse, get_object_or_404)
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.cache import cache_page
from django.views.generic import (ListView, DetailView, DeleteView,
                                  CreateView, UpdateView)
from rest_framework.parsers import MultiPartParser
from rest_framework.viewsets import ModelViewSet
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend

from accounts.models import UserProfile
from .common import save_csv_products
from .models import Product, Order, ProductImage
from .forms import ProductForm, GroupForm
from .serializers import ProductSerializer, OrderSerializer
from drf_spectacular.utils import extend_schema, OpenApiResponse

logger = logging.getLogger(__name__)


@extend_schema(description="Product views CRUD")
class ProductViewSet(ModelViewSet):
    """
    Набор представлений для действий над Product.

    Полный CRUD для сущностей товара.
    """

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [
        SearchFilter,
        DjangoFilterBackend,
        OrderingFilter,
    ]

    search_fields = [
        "name",
        "description"
    ]

    filterset_fields = [
        "id",
        "name",
        "description",
        "price",
        "discount",
        "archived",
    ]
    ordering_fields = [
        "id",
        "name",
        "price",
        "discount",
    ]

    @extend_schema(
        summary="Get one product by ID",
        description="Retrives **product**, returns 404 if not found",
        responses={
            200: ProductSerializer,
            404: OpenApiResponse(description="Empty response. Product by id not found"),
        }
    )
    def retrieve(self, *args, **kwargs):
        return super.retrieve(*args, **kwargs)

    @method_decorator(cache_page(60 * 2))
    def list(self, *args, **kwargs):
        print("Hello products list")
        return super().list(*args, **kwargs)

    @action(methods=["get"], detail=False)
    def download_csv(self, request: Request):
        response = HttpResponse(content_type="text/csv")
        filename = "products-export.csv"
        response["Content_Disposition"] = f"attachment; filename={filename}"
        queryset = self.filter_queryset(self.get_queryset())
        fields = [
            "id",
            "name",
            "description",
            "price",
            "discount",
        ]
        queryset = queryset.only(*fields)
        writer = DictWriter(response, fieldnames=fields)
        writer.writeheader()
        for product in queryset:
            writer.writerow({
                field: getattr(product, field)
                for field in fields
            })
        return response

    @action(methods=["post"], detail=False, parser_classes=[MultiPartParser], )
    def upload_csv(self, request: Request):
        products = save_csv_products(
            request.FILES["file"].file,
            encoding=request.encoding,
        )
        serializer = self.get_serializer(products, many=True)
        return Response(serializer.data)


class OrderViewSet(ModelViewSet):
    """
    Набор представлений для действий над Order.

    Полный CRUD для сущностей заказа.
    """

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = [
        SearchFilter,
        DjangoFilterBackend,
        OrderingFilter,
    ]

    search_fields = [
        "delivery_address",
        "user",
    ]

    filterset_fields = [
        "id",
        "delivery_address",
        "promocode",
        "created_at",
        "user",
        "products",
        "archived",
    ]
    ordering_fields = [
        "id",
        "delivery_address",
        "created_at",
        "user",
        "products",
    ]


class ShopIndexView(View):
    """Представление для главной страницы магазина."""

    # @method_decorator(cache_page(60 * 2))
    def get(self, request: HttpRequest) -> HttpResponse:
        """
        Обработчик GET запроса для отображения главной страницы магазина.

        :param request: HttpRequest объект запроса.
        :type request: HttpRequest
        :return: HttpResponse объект с отображением главной страницы магазина.
        :rtype: HttpResponse
        """
        products = [
            ("Laptop", 1299),
            ("Desktop", 1420),
            ("Smartphone", 399),
            ("Mouse", 15),
            ("Camera 'Tapo C310'", 12),
            ("USB Cable", 2.99),
        ]

        vacuum_cleaners = [
            ("Samsung", 356),
            ("Bosch", 412),
            ("Zelmer", 398),
            ("Philips", 356),
            ("Sony", 411),
            ("DeWalt", 465),
        ]
        context = {
            "time_running": round(default_timer(), 2),
            "products": products,
            "vacuumcleaners": vacuum_cleaners,
            "items": 5,
        }
        logger.debug("Products for shop index: %s", products, vacuum_cleaners)
        logger.info("Rendering shop index")
        print("shop index context", context)
        return render(request,
                      "shopapplication/shop-index.html", context=context)


class GroupsListView(View):
    """Представление для отображения списка групп пользователей."""

    def get(self, request: HttpRequest) -> HttpResponse:
        """
        Обработчик GET запроса для отображения списка групп пользователей.

        :param request: HttpRequest объект запроса.
        :type request: HttpRequest
        :return: HttpResponse объект ответа с отображением списка групп.
        :rtype: HttpResponse
        """
        groups = Group.objects.prefetch_related("permissions").all()
        context = {
            "form": GroupForm,
            "groups": groups,
        }
        return render(request,
                      "shopapplication/groups-list.html", context=context)

    def post(self, request: HttpRequest):
        """
        Обработчик POST запроса для создания новой группы пользователей.

        :param request: HttpRequest объект запроса.
        :type request: HttpRequest
        :return: HttpResponseRedirect объект перенаправления на
         текущую страницу после создания группы.
        :rtype: HttpResponseRedirect
        """
        form = GroupForm(request.POST)
        if form.is_valid():
            form.save()

        return redirect(request.path)


class ProductsDetailsView(DetailView):
    """Представление для отображения деталей продукта."""

    template_name = "shopapplication/product-details.html"
    # model = Product
    context_object_name = "product"
    queryset = Product.objects.all()


class ProductsListView(ListView):
    """Представление для отображения списка продуктов."""

    template_name = "shopapplication/products-list.html"
    # model = Product
    context_object_name = "products"
    queryset = Product.objects.filter(archived=False)


class OrdersListView(LoginRequiredMixin, ListView):
    """Представление для отображения списка заказов."""

    queryset = (Order.objects
                .select_related("user")
                .prefetch_related("products")
                )


class OrdersDetailView(PermissionRequiredMixin, DetailView):
    """Представление для отображения деталей заказа."""

    permission_required = ["shopapplication.view_order",
                           "shopapplication.view_user", ]
    queryset = (Order.objects
                .select_related("user")
                .prefetch_related("products")
                )


class ProductCreateView(CreateView):
    """Представление для создания нового продукта."""

    model = Product
    fields = ["name", "price", "discount",
              "description", "created_by", "preview"]
    success_url = reverse_lazy("shopapplication:products_list")


class OrderCreateView(PermissionRequiredMixin, CreateView):
    """Представление для создания нового заказа."""

    permission_required = "shopapplication.view_order"
    model = Order
    fields = "products", "delivery_address", "promocode", "user"
    success_url = reverse_lazy("shopapplication:order_list")


class ProductUpdateView(UserPassesTestMixin, UpdateView):
    """Представление для обновления информации о продукте."""

    model = Product
    # fields = ["name", "price", "discount", "description",
    # "created_by", "preview"]
    success_url = reverse_lazy("shopapplication:products_list")
    template_name = "shopapplication/product_update_form.html"
    form_class = ProductForm

    def test_func(self):
        """
        Проверяет, имеет ли текущий пользователь право редактировать.

        Данный продукт.

        :return: True, если пользователь является суперпользователем
        или имеет право изменять продукт и является создателем продукта;
        в противном случае - False.
        :rtype: bool
        """
        user = self.request.user
        product = self.get_object()
        return user.is_superuser or (user.has_perm
                                     ("shopapplication.change_product")
                                     and user == product.created_by)

    def form_valid(self, form):
        """
        Обрабатывает форму после успешной валидации.

        Создает объекты ProductImage для каждого изображения,
        загруженного в форме.

        :param form: Валидная форма.
        :type form: django.forms.Form
        :return: Ответ на запрос после успешной обработки формы.
        :rtype: django.http.HttpResponse
        """
        response = super().form_valid(form)
        for image in form.files.getlist("images"):
            ProductImage.objects.create(
                product=self.object,
                image=image,
            )
        return response


class OrderUpdateView(UpdateView):
    """Представление для обновления информации о заказе."""

    model = Order
    fields = "products", "delivery_address", "promocode", "user"
    template_name_suffix = "_update_form"

    def get_success_url(self):
        """
        Возвращает URL для перенаправления после успешного обновления заказа.

        :return: URL для перенаправления.
        :rtype: str
        """
        return reverse(
            "shopapplication:order_details",
            kwargs={"pk": self.object.pk},
        )


class ProductDeleteView(DeleteView):
    """Представление для удаления продукта."""

    model = Product
    success_url = reverse_lazy("shopapplication:products_list")

    def form_valid(self, form):
        """
        Обработка формы в случае ее корректного заполнения.

        Метод устанавливает флаг `archived` объекта.
        `self.object` в значение True,
        что позволяет "архивировать" объект после успешного
        сохранения формы.
        После этого происходит редирект на страницу, указанную в
        `success_url`.

        :param form: Валидная форма, представляющая объект.
        :type form: forms.Form
        :return: Редирект на страницу `success_url`.
        :rtype: HttpResponseRedirect
        """
        success_url = self.get_success_url()
        self.object.archived = True
        self.object.save()
        return HttpResponseRedirect(success_url)


class OrderDeleteView(DeleteView):
    """Представление для удаления заказа."""

    model = Order
    success_url = reverse_lazy("shopapplication:order_list")


class ProductsDataExportView(View):
    """Представление для экспорта данных о продуктах."""

    def get(self, request: HttpRequest) -> JsonResponse:
        """
        Обработчик GET-запроса для получения данных о продуктах.

        Извлекает все продукты из базы данных, сортирует их по
        первичному ключу и формирует список словарей с данными о
        каждом продукте.
        Затем возвращает JsonResponse с данными о продуктах.

        :param request: HttpRequest объект, представляющий запрос.
        :type request: HttpRequest
        :return: JsonResponse с данными о продуктах.
        :rtype: JsonResponse
        """
        cache_key = "products_data_export"
        products_data = cache.get(cache_key)
        if products_data is None:
            products = Product.objects.order_by("pk").all()
            products_data = [
                {
                    "pk": product.pk,
                    "name": product.name,
                    "price": product.price,
                    "archived": product.archived,
                }
                for product in products
            ]
        elem = products_data[0]
        name = elem["name"]
        print("name:", name)
        cache.set(cache_key, products_data, 300)
        return JsonResponse({"products": products_data})


class OrdersExportView(View):
    """Представление для экспорта данных о продуктах."""

    def get(self, request, *args, **kwargs):
        """
        Обработчик GET-запроса для получения списка заказов.

        :param request: Объект запроса.
        :type request: HttpRequest
        :param args: Позиционные аргументы.
        :type args: tuple
        :param kwargs: Именованные аргументы.
        :type kwargs: dict
        :return: JsonResponse с данными о заказах.
        :rtype: JsonResponse
        """
        orders = Order.objects.all().values("id", "delivery_address",
                                            "promocode", "user_id")
        data = {"orders": list(orders)}
        return JsonResponse(data)


class LatestProductsFeed(Feed):
    title = "Products (latest)"
    description = "Updates on changes and addition products"

    def link(self):
        return reverse("shopapplication:products-list")

    def items(self):
        return (Product.objects
                .filter(created_at__isnull=False)
                .order_by("-created_at")[:5]
                )

    def item_title(self, item: Product):
        return item.name

    def item_description(self, item: Product):
        description = item.description[:10] if item.description else ""
        return f"{description} - Price: {item.price}"


class UserOrdersListView(LoginRequiredMixin, ListView):
    """Представление для отображения списка заказов конкретного пользователя."""

    template_name = "shopapplication/user_orders_list.html"
    context_object_name = "orders"
    paginate_by = 10

    def get_queryset(self):
        user_id = self.kwargs.get("user_id")
        self.user = get_object_or_404(User, pk=user_id)
        return Order.objects.filter(user=self.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["owner"] = self.user
        return context


class UserOrdersExportView(View):
    def get(self, request, user_id):
        cache_key = f"user_orders_export_{user_id}"
        cached_data = cache.get(cache_key)
        if cached_data is not None:
            return JsonResponse(cached_data, safe=False)

        user = get_object_or_404(User, pk=user_id)
        orders = Order.objects.filter(user=user).order_by("pk")
        serializer = OrderSerializer(orders, many=True)
        data = serializer.data
        cache.set(cache_key, data, 60 * 2)
        return JsonResponse(data, safe=False)
