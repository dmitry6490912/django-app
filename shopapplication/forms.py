from django import forms
from django.contrib.auth.models import Group
from django.forms import ModelForm

from .models import Product, Order


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ("name", "price", "discount", "description", "created_by", "preview")

    images = forms.ImageField(
        widget=forms.ClearableFileInput(),
    )


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ("products", "delivery_address", "promocode", "user", "receipt")


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ("name",)


class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    file = forms.FileField()


class CSVImportForm(forms.Form):
    csv_file = forms.FileField()
