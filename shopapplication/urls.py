from django.urls import path, include
from django.views.decorators.cache import cache_page
from rest_framework.routers import DefaultRouter
from .views import (ShopIndexView, GroupsListView, OrdersListView, OrdersDetailView,
                    OrderCreateView, ProductsDetailsView, ProductsListView, ProductCreateView,
                    ProductUpdateView, ProductDeleteView, OrderUpdateView, OrderDeleteView,
                    ProductsDataExportView, OrdersExportView, ProductViewSet, OrderViewSet,
                    LatestProductsFeed, UserOrdersListView, UserOrdersExportView)

routers = DefaultRouter()
routers.register("products", ProductViewSet)
routers.register("orders", OrderViewSet)
app_name = "shopapplication"


urlpatterns = [
    path("api/", include(routers.urls)),
    path("", ShopIndexView.as_view(), name="index"),
    path("groups/", GroupsListView.as_view(), name="groups_list"),
    path("products/", ProductsListView.as_view(), name="products_list"),
    path("products/<int:pk>/", ProductsDetailsView.as_view(), name="product_details"),
    path("products/create/", ProductCreateView.as_view(), name="product_create"),
    path("products/<int:pk>/update/", ProductUpdateView.as_view(), name="product_update"),
    path("products/<int:pk>/archive/", ProductDeleteView.as_view(), name="product_delete"),
    path("products/export/", ProductsDataExportView.as_view(), name="products_export"),
    path("orders/", OrdersListView.as_view(), name="order_list"),
    path("orders/<int:pk>/", OrdersDetailView.as_view(), name="order_details"),
    path("orders/create/", OrderCreateView.as_view(), name="order_create"),
    path("orders/<int:pk>/update/", OrderUpdateView.as_view(), name="order_update"),
    path("orders/<int:pk>/delete/", OrderDeleteView.as_view(), name="order_delete"),
    path('orders/export/', OrdersExportView.as_view(), name='orders_export'),
    path("products/latest/feed/", LatestProductsFeed(), name="latest-products-feed"),
    path("users/<int:user_id>/orders/", UserOrdersListView.as_view(), name="user_orders_list"),
    path("users/<int:user_id>/orders/export/", UserOrdersExportView.as_view(), name="user_orders_export"),
]
