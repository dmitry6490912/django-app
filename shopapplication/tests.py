from django.conf import settings
from django.contrib.auth.models import User, Permission
from django.test import TestCase
from django.urls import reverse
from string import ascii_letters
from random import choices

from .models import Product, Order
from .utils import add_two_numbers


class AddTwoNumbersTestCase(TestCase):
    def test_add_two_numbers(self):
        result = add_two_numbers(3, 6)
        self.assertEqual(result, 9)


class ProductCreateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.product_name = "".join(choices(ascii_letters, k=20))
        Product.objects.filter(name=self.product_name).delete()
        self.user = User.objects.create_user(username="Alex", password="08170817")
        permission = Permission.objects.get(codename="add_product")
        self.user.user_permissions.add(permission)
        self.client.force_login(self.user)

    def test_create_product(self):
        headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}
        response = self.client.post(reverse("shopapplication:product_create"),
                                    {
                                        "name": self.product_name,
                                        "price": "123.45",
                                        "discount": "12",
                                        "description": "Best",
                                        "created_by": self.user.id,
                                    }, **headers)
        self.assertRedirects(response, reverse("shopapplication:products_list"), status_code=302)
        self.assertTrue(
            Product.objects.filter(name=self.product_name).exists()
        )


class ProductDetailsViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.product = Product.objects.create(name="Best product")

    @classmethod
    def tearDownClass(cls):
        cls.product.delete()

    def test_get_product(self):
        headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}
        response = self.client.get(reverse("shopapplication:product_details", kwargs={"pk": self.product.pk}), **headers
                                   )
        self.assertEqual(response.status_code, 200)

    def test_get_product_and_check_content(self):
        headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}
        response = self.client.get(reverse("shopapplication:product_details", kwargs={"pk": self.product.pk}), **headers
                                   )
        self.assertContains(response, self.product.name)


class ProductsListViewTestCase(TestCase):
    fixtures = [
        "products-fixture.json", "users-fixture.json"
    ]

    def test_products(self):
        headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}
        response = self.client.get(reverse("shopapplication:products_list"), **headers)
        self.assertQuerysetEqual(
            qs=Product.objects.filter(archived=False).all(),
            values=(p.pk for p in response.context["products"]),
            transform=lambda p: p.pk
        )
        self.assertTemplateUsed(response, "shopapplication/products-list.html")


class OrdersListViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.credentials = dict()
        cls.user = User.objects.create_user(username="Tom", password="123456")

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def setUp(self) -> None:
        self.client.force_login(self.user)

    def test_orders_view(self):
        headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}
        response = self.client.get(reverse("shopapplication:order_list"), **headers)
        self.assertContains(response, "Orders")

    def test_orders_view_not_authenticated(self):
        headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}
        self.client.logout()
        response = self.client.get(reverse("shopapplication:order_list"), **headers)
        self.assertEqual(response.status_code, 302)
        self.assertIn(str(settings.LOGIN_URL), response.url)


class ProductsExportViewTestCase(TestCase):
    fixtures = ["users-fixture.json", "products-fixture.json"]

    def test_get_products_view(self):
        response = self.client.get(
            reverse("shopapplication:products_export")
        )
        self.assertEqual(response.status_code, 200)
        products = Product.objects.order_by("pk").all()
        expected_data = [
            {
                "pk": product.pk,
                "name": product.name,
                "price": str(product.price),
                "archived": product.archived,
            }
            for product in products
        ]
        products_data = response.json()
        self.assertEqual(
            products_data["products"],
            expected_data,
        )


class OrderDetailViewTestCase(TestCase):
    fixtures = ["users-fixture.json", "orders-fixture.json", "products-fixture.json"]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.order = Order.objects.first()
        cls.user = User.objects.get(username="Alex")
        cls.permission = Permission.objects.get(codename="view_order")
        cls.user.user_permissions.add(cls.permission)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def setUp(self):
        self.user = User.objects.get(username="admin")
        self.client.force_login(self.user)

    def tearDown(self):
        self.client.logout()

    def test_order_details(self):
        response = self.client.get(reverse("shopapplication:order_details", kwargs={"pk": self.order.pk}))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.order.delivery_address)
        self.assertContains(response, self.order.promocode)
        self.assertEqual(response.context["object"], self.order)


class OrdersExportTestCase(TestCase):
    fixtures = ["users-fixture.json", "orders-fixture.json", "products-fixture.json"]


    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def setUp(self):
        self.user = User.objects.get(username="admin")
        self.client.login(username="admin", password="08170602")

    def tearDown(self):
        self.client.logout()

    def test_orders_export(self):
        headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}
        response = self.client.get(reverse("shopapplication:orders_export"), **headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue("orders" in data)

        self.assertTrue("id" in data["orders"][0])
        self.assertTrue("delivery_address" in data["orders"][0])
        self.assertTrue("promocode" in data["orders"][0])
        self.assertTrue("user_id" in data["orders"][0])
        self.assertFalse("product_ids" in data["orders"][0])

        self.assertTrue("id" in data["orders"][1])
        self.assertTrue("delivery_address" in data["orders"][1])
        self.assertTrue("promocode" in data["orders"][1])
        self.assertTrue("user_id" in data["orders"][1])
        self.assertFalse("product_ids" in data["orders"][1])
