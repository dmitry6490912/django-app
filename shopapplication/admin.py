from django.contrib import admin
from django.db.models import QuerySet
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, redirect
from django.urls import path

from .common import save_csv_products, save_csv_orders
from .models import Product, Order, ProductImage
from .admin_mixins import ExportAsCSVMixin
from .forms import CSVImportForm


class OrderInLine(admin.TabularInline):
    model = Product.orders.through


class ProductImageInline(admin.StackedInline):
    model = ProductImage
    extra = 1


@admin.action(description="Archive products")
def mark_archived(modeladmin: admin.ModelAdmin, request: HttpRequest, queryset: QuerySet):
    queryset.update(archived=True)


@admin.action(description="Unarchive products")
def mark_unarchived(modeladmin: admin.ModelAdmin, request: HttpRequest, queryset: QuerySet):
    queryset.update(archived=False)


@admin.action(description="Archive orders")
def mark_orders_archived(modeladmin: admin.ModelAdmin, request: HttpRequest, queryset: QuerySet):
    queryset.update(archived=True)


@admin.action(description="Unarchive orders")
def mark_orders_unarchived(modeladmin: admin.ModelAdmin, request: HttpRequest, queryset: QuerySet):
    queryset.update(archived=False)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin, ExportAsCSVMixin):
    actions = [
        mark_archived,
        mark_unarchived,
        "export_csv",
    ]
    inlines = [
        OrderInLine,
        ProductImageInline,
    ]
    # list_display = "pk", "name", "description", "price", "discount"
    list_display = "pk", "name", "description_short", "price", "discount", "archived", "created_by", "preview"
    list_display_links = "pk", "name"
    ordering = "pk", "price"
    search_fields = "name", "description"
    fieldsets = [
        (None, {
            "fields": ("name", "description", "created_by")
        }),
        ("Price options", {
            "fields": ("price", "discount"),
            "classes": ("wide", "collapse"),
        }),
        ("Images", {
            "fields": ("preview",),
        }),
        ("Extra options", {
            "fields": ("archived",),
            "classes": ("wide", "collapse"),
            "description": "Extra options. Field 'archived' is for soft delete.",
        }),
    ]

    def description_short(self, obj: Product) -> str:
        if obj.description is None or len(obj.description) <= 21:
            return obj.description
        return obj.description[:15] + "..."

    change_list_template = "shopapplication/products_changelist.html"

    def import_csv(self, request: HttpRequest) -> HttpResponse:
        if request.method == "GET":
            form = CSVImportForm()
            context = {
                "form": form,
            }
            return render(request, "admin/csv_form.html", context)
        form = CSVImportForm(request.POST, request.FILES)
        if not form.is_valid():
            context = {
                "form": form,
            }
            return render(request, "admin/csv_form.html", context, status=400)

        save_csv_products(
            file=form.files["csv_file"].file,
            encoding=request.encoding,
        )
        self.message_user(request, "Data from CSV was imported")
        return redirect("..")

    def get_urls(self):
        urls = super().get_urls()
        new_urls = [
            path(
                "import-products-csv/",
                self.import_csv,
                name="import_products_csv",
            ),
        ]
        return new_urls + urls


# admin.site.register(Product, ProductAdmin)


# class ProductInLine(admin.TabularInline):
class ProductInLine(admin.StackedInline):
    model = Order.products.through


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin, ExportAsCSVMixin):
    actions = [
        mark_orders_archived,
        mark_orders_unarchived,
        "export_csv",
    ]
    inlines = [
        ProductInLine,
    ]
    list_display = "delivery_address", "promocode", "created_at", "user_verbose", "archived", "receipt"
    list_display_links = "delivery_address", "created_at"
    ordering = "delivery_address", "promocode"
    search_fields = "delivery_address",
    fieldsets = [
        (None, {
            "fields": ("user", "delivery_address", "receipt",),
        }),
        ("Delivery options", {
            "fields": ("promocode",),
            "classes": ("wide", "collapse"),
        }),
        ("Additional options", {
            "fields": ("archived",),
            "classes": ("wide", "collapse"),
            "description": "Additional options. 'Archived' field is for soft delete.",
        }),
    ]

    def get_queryset(self, request):
        return Order.objects.select_related("user").prefetch_related("products")

    def get_queryset(self, request):
        return Order.objects.select_related("user").prefetch_related("products")

    def user_verbose(self, obj: Order) -> str:
        return obj.user.first_name or obj.user.username

    change_list_template = "shopapplication/orders_changelist.html"

    def import_csv(self, request: HttpRequest) -> HttpResponse:
        if request.method == "GET":
            form = CSVImportForm()
            context = {
                "form": form,
            }
            return render(request, "admin/csv_form.html", context)
        form = CSVImportForm(request.POST, request.FILES)
        if not form.is_valid():
            context = {
                "form": form,
            }
            return render(request, "admin/csv_form.html", context, status=400)

        save_csv_orders(
            file=form.files["csv_file"].file,
            encoding=request.encoding
        )
        self.message_user(request, "Data from CSV was imported")
        return redirect("..")

    def get_urls(self):
        urls = super().get_urls()
        new_urls = [
            path(
                "import-orders-csv/",
                self.import_csv,
                name="import_orders_csv",
            ),
        ]
        return new_urls + urls
