from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class ShopSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return [
            "shopapplication:product-list",
            "shopapplication:order_list",
        ]

    def location(self, item):
        return reverse(item)

