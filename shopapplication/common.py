from csv import DictReader
from io import TextIOWrapper

from django.contrib.auth.models import User

from shopapplication.models import Product, Order


def save_csv_products(file, encoding):
    csv_file = TextIOWrapper(
        file,
        encoding=encoding,
    )
    reader = DictReader(csv_file)
    products = [
        Product(**row)
        for row in reader
    ]
    Product.objects.bulk_create(products)
    return products


def save_csv_orders(file, encoding):
    csv_file = TextIOWrapper(file, encoding=encoding)
    reader = DictReader(csv_file)
    orders = []
    products_cache = {product.name: product for product in Product.objects.all()}
    users_cache = {user.username: user for user in User.objects.all()}

    for row in reader:
        product_names = row.pop("products").split(", ")
        username = row.pop("user")
        user = users_cache.get(username)

        if not user:
            print(f"User '{username}' not found in the database.")
            continue

        order_data = {**row, "user": user}
        order = Order(**order_data)
        order.save()

        for product_name in product_names:
            product = products_cache.get(product_name)
            if not product:
                print(f"Product '{product_name}' not found in the database.")
                continue
            order.products.add(product)

        orders.append(order)

    return orders

