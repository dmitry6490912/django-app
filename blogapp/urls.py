from django.urls import path
from .views import (ClassBasedView, ArticleDetailView,
                    ArticleListView, LatestArticlesFeed)

app_name = "blogapp"

urlpatterns = [
    path("article_list/", ClassBasedView.as_view(), name="blog"),
    path("articles/", ArticleListView.as_view(), name="articles"),
    path("article/<int:pk>/", ArticleDetailView.as_view(), name="article"),
    path("articles/latest/feed/", LatestArticlesFeed(), name="articles-feed"),
]
