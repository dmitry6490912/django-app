from django.core.management import BaseCommand
from django.utils import timezone
from blogapp.models import Article, Author, Category, Tag


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        self.stdout.write("Write to the database five example articles")
        author = Author.objects.create(name="P. Fortran", bio="Professor")
        category = Category.objects.create(name="Technical info")
        tags = [Tag.objects.create(name="Python"), Tag.objects.create(name="Django")]

        for articl in range(5):
            article = Article.objects.create(
                title=f"Article {articl+1}",
                content=f"Content of article {articl+1}",
                pub_date=timezone.now(),
                author=author,
                category=category
            )
            article.tags.add(*tags)
