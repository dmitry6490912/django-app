from django.test import TestCase
from django.urls import reverse

from .views import get_cookie_view


class GetCookieViewTestCase(TestCase):
    def test_get_cookie_view(self):
        headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}
        response = self.client.get(reverse("accounts:cookie-get"), **headers)
        self.assertContains(response, "Cookie value", status_code=200)


class FooBarTestCase(TestCase):
    def test_foo_bar_view(self):
        headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}
        response = self.client.get(reverse("accounts:foo-bar"), **headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers["content-type"], "application/json",)
        expected_data = b'{"foo": "bar", "spam": "eggs"}'
        self.assertEqual(response.content, expected_data)
