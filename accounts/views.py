from random import random
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.views import LogoutView, LoginView
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView, CreateView, ListView, DetailView
from django.utils.translation import gettext_lazy as _, ngettext

from accounts.models import AvatarForm, UserProfile


class HelloView(View):
    welcome_message = _("welcome hello world!")

    def get(self, request: HttpRequest) -> HttpResponse:
        items_string = request.GET.get("items") or 0
        items = int(items_string)
        products_line = ngettext(
            "one product",
            "{count} products",
            items,
        )
        products_line = products_line.format(count=items)
        return HttpResponse(
            f"<h1>{self.welcome_message}</h1>"
            f"\n<h2>{products_line}</h2>"
        )


class AboutMeView(TemplateView):
    template_name = "accounts/about-me.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        profile, created = UserProfile.objects.get_or_create(user=self.request.user)
        context["form"] = AvatarForm(instance=profile)
        context["user_profile"] = profile
        return context


class UpdateAvatarView(View):
    def post(self, request, *args, **kwargs):
        profile = get_object_or_404(UserProfile, user=request.user)

        form = AvatarForm(request.POST, request.FILES, instance=profile)
        if form.is_valid():
            form.save()
            messages.success(request, "Avatar updated successfully!")
            return redirect("accounts:about-me")
        else:
            messages.error(request, "Error updating avatar.")
            return render(request, "accounts/about-me.html", {"form": form})


class UserListView(ListView):
    model = UserProfile
    template_name = "accounts/user_list.html"
    context_object_name = "profiles_list"


class UserDetailView(DetailView):
    model = UserProfile
    template_name = "accounts/user_detail.html"
    context_object_name = "user_profile"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = AvatarForm(instance=self.object)
        return context


def is_admin_or_owner(user, profile_id):
    return user.is_staff or user.profile.id == profile_id


@method_decorator(user_passes_test(lambda u, pk=None: is_admin_or_owner(u, pk)), name="dispatch")
class UserProfileDetailView(DetailView):
    model = UserProfile
    template_name = "accounts/user_detail.html"
    context_object_name = "profile"

    def post(self, request, *args, **kwargs):
        profile = self.get_object()
        form = AvatarForm(request.POST, request.FILES, instance=profile)
        if form.is_valid():
            form.save()
            return redirect('accounts:user-detail', pk=profile.id)
        return render(request, self.template_name, {'form': form, 'profile': profile})


class RegisterView(CreateView):
    form_class = UserCreationForm
    template_name = "accounts/register.html"
    success_url = reverse_lazy("accounts:about-me")

    def form_valid(self, form):
        response = super().form_valid(form)
        user_profile, created = UserProfile.objects.get_or_create(user=self.object)

        self.object.user_profile = user_profile
        self.object.save()

        return response


class MyLoginView(LoginView):
    template_name = "accounts/login.html"
    redirect_authenticated_user = True


class MyLogoutView(LogoutView):
    def dispatch(self, request, *args, **kwargs):
        if request.method == "GET":
            return self.get(request, *args, **kwargs)
        return super().dispatch(request, *args, **kwargs)

    next_page = reverse_lazy("accounts:login")


@user_passes_test(lambda u: u.is_superuser)
def set_cookie_view(request: HttpRequest) -> HttpResponse:
    response = HttpResponse("Cookie set")
    response.set_cookie("fizz", "buzz", max_age=3600)
    return response


@cache_page(timeout=60 * 2)
def get_cookie_view(request: HttpRequest) -> HttpResponse:
    value = request.COOKIES.get("fizz", "default value")
    return HttpResponse(f"Cookie value: {value!r} + {random()}")


@permission_required("accounts.view_profile", raise_exception=True)
def set_session_view(request: HttpRequest) -> HttpResponse:
    request.session["foobar"] = "spameggs"
    return HttpResponse("Session set!")


@login_required
def get_session_view(request: HttpRequest) -> HttpResponse:
    value = request.session.get("foobar", "default")
    return HttpResponse(f"Session value {value!r}")


class FooBarView(View):
    def get(self, request: HttpRequest) -> JsonResponse:
        return JsonResponse({"foo": "bar", "spam": "eggs"})
