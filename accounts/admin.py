from django.contrib import admin
from django.contrib.auth.models import Permission, User
from django.contrib.contenttypes.models import ContentType

from .models import UserProfile


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ["user", "biography", "agreement_accepted", "avatar"]
    list_filter = ["agreement_accepted"]
    readonly_fields = ["user"]

    def save_model(self, request, obj, form, change):
        if not obj.user_id:
            obj.user = request.user
        obj.save()

        content_type = ContentType.objects.get_for_model(UserProfile)
        permission, created = Permission.objects.get_or_create(
            codename="add_profile",
            name="Can add profile",
            content_type=content_type,
        )
        obj.user.user_permissions.add(permission)
