from django.contrib.auth.views import LoginView
from django.urls import path
from .views import (get_cookie_view, set_cookie_view, set_session_view, get_session_view, MyLogoutView,
                    MyLoginView, AboutMeView, RegisterView, FooBarView, UserListView, UserDetailView,
                    UpdateAvatarView, HelloView)

app_name = "accounts"

urlpatterns = [
    # path("login/",
    #      LoginView.as_view(
    #          template_name="accounts/login.html",
    #          redirect_authenticated_user=True,
    #      ),
    #      name="login"),
    # path("logout/", logout_view, name="logout"),
    path("login/", MyLoginView.as_view(), name="login"),
    path("hello/", HelloView.as_view(), name="hello"),
    path("logout/", MyLogoutView.as_view(), name="logout"),
    path("cookie/get/", get_cookie_view, name="cookie-get"),
    path("cookie/set/", set_cookie_view, name="cookie-set"),
    path("session/get/", get_session_view, name="session-get"),
    path("session/set/", set_session_view, name="session-set"),
    path("about-me/", AboutMeView.as_view(), name="about-me"),
    path("update_avatar/<int:pk>/", UpdateAvatarView.as_view(), name="update-avatar"),
    path("users/", UserListView.as_view(), name="user-list"),
    path("user/<int:pk>/", UserDetailView.as_view(), name="user-detail"),
    path("register/", RegisterView.as_view(), name="register"),
    path("foo-bar/", FooBarView.as_view(), name="foo-bar"),
]
